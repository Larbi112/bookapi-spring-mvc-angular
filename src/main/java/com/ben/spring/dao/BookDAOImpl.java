package com.ben.spring.dao;

import com.ben.spring.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookDAOImpl implements BookDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public long save(Book book) {
        sessionFactory.getCurrentSession().save(book);
        return book.getId();
    }

    public Book get(long id) {
         return  (Book) sessionFactory.getCurrentSession().get(Book.class,id);
    }

    public List<Book> list() {
       List<Book>list= sessionFactory.getCurrentSession().createQuery("from Book").list();
       return  list;
    }

    public void update(long id, Book book) {
        Session session=sessionFactory.getCurrentSession();
        Book oldBook= (Book) session.byId(Book.class).load(id);
        oldBook.setTitle(book.getTitle());
        oldBook.setAuthor(book.getAuthor());
        session.flush();
    }

    public void delete(long id) {
    Session session=sessionFactory.getCurrentSession();
    Book book= (Book) session.byId(Book.class).load(id);
    session.delete(book);
    }
}
