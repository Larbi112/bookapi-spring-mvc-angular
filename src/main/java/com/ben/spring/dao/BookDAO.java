package com.ben.spring.dao;

import com.ben.spring.model.Book;

import java.util.List;

public interface BookDAO {
    //save the record
    long save(Book book);

    // get a single record
    Book get(long id);

    // get all the records
    List<Book> list();

    //update the record
    void update(long id,Book book);

    //Delete a record
    void delete(long id);

}
