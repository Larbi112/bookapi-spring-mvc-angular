package com.ben.spring.service;

import com.ben.spring.dao.BookDAO;
import com.ben.spring.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDAO bookDAO;

    @Transactional
    public long save(Book book) {
        return bookDAO.save(book);
    }
    @Transactional
    public Book get(long id) {
        return bookDAO.get(id);
    }

    @Transactional
    public List<Book> list() {
        return bookDAO.list();
    }

    @Transactional
    public void update(long id, Book book) {
        bookDAO.update(id,book);

    }

    @Transactional
    public void delete(long id) {
    bookDAO.delete(id);
    }
}
